Abbas Khan - CT6025 Graphics Programming Notice

- Project Details
Target Platform Version: Windows 10
Platform Toolset: Visual Studio 2017 v140
Project Configuration: Debug, x64

- Audio
The audio used in this assignment is licensed under CC 3.0 and the details for this can be found below.

"Midnight Tale" Kevin MacLeod (incompetech.com)
Licensed under Creative Commons: By Attribution 3.0 License
http://creativecommons.org/licenses/by/3.0/

"Stringed Disco" Kevin MacLeod (incompetech.com)
Licensed under Creative Commons: By Attribution 3.0 License
http://creativecommons.org/licenses/by/3.0/
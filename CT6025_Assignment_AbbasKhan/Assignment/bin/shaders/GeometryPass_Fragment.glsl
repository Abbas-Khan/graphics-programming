///////////////////////////////////////////////////////////////////
// Name: GeometryPass_Fragment.glsl
// Author: Abbas Khan
// Bio: Fragment shader for the geometry pass
///////////////////////////////////////////////////////////////////
#version 150

// Take in the vertex structure from the vertex shader
in Vertex
{
	// Vector 4 to store the position
	vec4 vec4Position;
	// Vector 4 to store the normal
	vec4 vec4Normal;
	// Vector 2 to store the UV
	vec2 vec2UV;
}xVertex;

// Output three vector 4 variables to create 3 texture maps that can be sampled at a later point
out vec4 vec4RTDiffuse;
out vec4 vec4RTNormal;
out vec4 vec4RTPosition;

// Uniform to get the diffuse texture for the model
uniform sampler2D s2DDiffuseTexture;
 
 // Main Function
void main()
{
	// Sample from the Diffuse texture at the given UV point and assign it to the vector 4
	vec4RTDiffuse = texture(s2DDiffuseTexture, xVertex.vec2UV);
	// Assign the normal value
	vec4RTNormal = vec4((xVertex.vec4Normal.xyz + 1.0) * 0.5, 1.0);
	// Assign the normal value
	vec4RTPosition = xVertex.vec4Position;
}
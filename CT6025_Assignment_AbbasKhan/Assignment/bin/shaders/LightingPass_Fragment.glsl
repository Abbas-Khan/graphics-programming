///////////////////////////////////////////////////////////////////
// Name: LightingPass_Fragment.glsl
// Author: Abbas Khan
// Bio: Fragment shader for the lighting pass
///////////////////////////////////////////////////////////////////
#version 150

// Input from vertex shader
in vec2 vec2UVoutput;
	
// Output a vector 4 variable to create a texture map that can be sampled at a later point
out vec4 vec4RTLighting;

// Uniforma to get the normal & position textures for the model
uniform sampler2D s2DNormalTexture;
uniform sampler2D s2DPositionTexture;

// Uniform to get the camera's position
uniform vec3 vec3CameraPosition;

// Uniform to get the size of the render target
uniform vec2 vec2RenderTargetSize;

// Structure for the light containing the position, colour, intensity & radius
struct Light
{
	vec3 vec3LightPosition;
	vec3 vec3LightColour_Diffuse;
	vec3 vec3LightColour_Specular;
	vec3 vec3LightColour_Ambient;
	float fLightIntensity;
	float fLightRadius;
};

// Uniform to get all the light's within the scene
uniform Light xLights[32];

// Uniform to get the amount of lights active in the scene
uniform int iAmountOfActiveLights;

// Point Attenuation Variables
float fConstant = 0.0f;
float fLinear = 0.0f;
float fQuadratic = 0.3f;

// Function used to calculate the light colour to be emitted from a light
vec4 CalcLightInternal(Light a_xTargetLight, vec3 a_vec3LightDirection, vec3 a_vec3Position, vec3 a_vec3Normal)
{
	// Vector 4 variables used to store the diffuse, specular & ambient colour of the light
    vec4 vec4DiffuseColor = vec4(a_xTargetLight.vec3LightColour_Diffuse.x, a_xTargetLight.vec3LightColour_Diffuse.y, a_xTargetLight.vec3LightColour_Diffuse.z, 0);
    vec4 vec4SpecularColor = vec4(a_xTargetLight.vec3LightColour_Specular.x, a_xTargetLight.vec3LightColour_Specular.y, a_xTargetLight.vec3LightColour_Specular.z, 0);
    vec4 vec4AmbientColor = vec4(a_xTargetLight.vec3LightColour_Ambient.xyz * a_xTargetLight.fLightIntensity, 1.0);

	// Float variable used to calculate the diffuse factor
    float fDiffuseFactor = dot(a_vec3Normal, -a_vec3LightDirection);

	// If it is greater than 0
    if (fDiffuseFactor > 0.0)
	{
		// Re-calculate the diffuse colour
        vec4DiffuseColor = vec4(a_xTargetLight.vec3LightColour_Diffuse.xyz * a_xTargetLight.fLightIntensity * fDiffuseFactor, 1.0);

		// Variables used to calculate the specular factor
        vec3 vec3VertexToEye = normalize(vec3CameraPosition - a_vec3Position);
		vec3 vec3LightReflect = normalize(reflect(a_vec3LightDirection, a_vec3Normal));
       
		// Float variable used to calculate the diffuse factor
	   float fSpecularFactor = dot(vec3VertexToEye, vec3LightReflect);    
	   
		// If it is greater than 0
        if (fSpecularFactor > 0.0)
		{
			// Set the specular factor
            fSpecularFactor = pow(fSpecularFactor, 1);
			// Re-calculate the specular colour
            vec4SpecularColor = vec4(a_xTargetLight.vec3LightColour_Specular.xyz * a_xTargetLight.fLightIntensity * fSpecularFactor, 1.0);
        }
    }

	// Return the colours added together
    return (vec4AmbientColor + vec4DiffuseColor + vec4SpecularColor);
}

// Function used to calculate the final light colour from a point light
vec4 CalcPointLight(Light a_xTargetLight, vec3 a_vec3Position, vec3 a_vec3Normal)
{
	// Vector 3 to hold the light's direction
    vec3 v3LightDir = a_vec3Position - a_xTargetLight.vec3LightPosition;
	
	// Float to calculate the distance of the light
    float fDistance = length(v3LightDir);

	// Normalise the light direction
    v3LightDir = normalize(v3LightDir);

	// Get the colour of the light
    vec4 vec4Color = CalcLightInternal(a_xTargetLight, v3LightDir, a_vec3Position, a_vec3Normal);

	// Attenuation calculation
    float fAttenuation = fConstant + fLinear * fDistance + fQuadratic * fDistance * fDistance;

	// Attenuation scaling
    fAttenuation = max(1.0, fAttenuation);

	// Return the final colour with the attenuation applied
    return vec4Color / fAttenuation;
}

 // Main Function
void main()
{
	// Variable to store the UV of the fragment in relation to the render target size
	vec2 vec2FragmentUV = gl_FragCoord.xy / vec2RenderTargetSize;
	vec3 vec3Position = texture(s2DPositionTexture, vec2FragmentUV).xyz;

	// Loop through all the lights
	for(int i = 0; i < iAmountOfActiveLights; i++)
	{
		// Calculate the distance from the light to the fragment's position
		float fDistance = length(xLights[i].vec3LightPosition - vec3Position);

		// If the distance is within the light's radius
		if(fDistance < xLights[i].fLightRadius)
		{
			// Vector 3 to get the normal from the texture
			vec3 vec3Normal = (texture(s2DNormalTexture, vec2FragmentUV).xyz * 2.0) - 1.0;
			// Normalise the normal
			vec3 vec3LightNormal = normalize(vec3Normal);
			// Calculate the color from the point light & output the result to the lighting RT
			vec4RTLighting += CalcPointLight(xLights[i], vec3Position, vec3LightNormal);
		}
	}
}
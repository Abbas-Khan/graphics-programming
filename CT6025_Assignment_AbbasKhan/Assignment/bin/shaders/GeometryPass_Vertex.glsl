///////////////////////////////////////////////////////////////////
// Name: GeometryPass_Vertex.glsl
// Author: Abbas Khan
// Bio: Vertex shader for the geometry pass
///////////////////////////////////////////////////////////////////
#version 150

// Pass in variables for the Position (Vector 4), Normal (Vector 4) & UV (Vector 2)
in vec4 vec4Position;
in vec4 vec4Normal;
in vec2 vec2UV;

// Output a vertex structure containing all the Position (Vector 4), Normal (Vector 4) & UV (Vector 2)
out Vertex
{
	// Vector 4 to store the position
	vec4 vec4Position;
	// Vector 4 to store the normal
	vec4 vec4Normal;
	// Vector 2 to store the UV
	vec2 vec2UV;
}xVertex;

// Uniform variables used to gather information from the application
uniform mat4 mat4ProjectionView; 
uniform mat4 mat4Model;
uniform mat4 mat4NormalMatrix;

// Main Function
void main() 
{ 
	// Assign the Position value to the passed in position relative to the model matrix
	xVertex.vec4Position = mat4Model * vec4Position;
	// Assign the Normal value to the passed in normal relative to the normal matrix
	xVertex.vec4Normal = mat4NormalMatrix * vec4Normal;
	// Assign the UV value to the passed in uv
	xVertex.vec2UV = vec2UV;
	// Assign the GL Position
	gl_Position = mat4ProjectionView * mat4Model * vec4Position;
}
///////////////////////////////////////////////////////////////////
// Name: CompositePass_Fragment.glsl
// Author: Abbas Khan
// Bio: Fragment shader for the lighting pass
///////////////////////////////////////////////////////////////////
#version 150

// Input from vertex shader
in vec2 vec2UVoutput;

// Output the Fragment's colour
out vec4 vec4FragmentColour;

// Uniforma to get the diffuse & lighting textures for the model
uniform sampler2D s2DDiffuseTexture;
uniform sampler2D s2DLightingTexture;

// Uniform to get the size of the render target
uniform vec2 vec2RenderTargetSize;

 // Main Function
void main()
{
	// Variable to store the UV of the fragment in relation to the render target size 
	vec2 vec2FragmentUV = gl_FragCoord.xy / vec2RenderTargetSize;

	// Variable used to sample the diffuse texture at the gicen UV
	vec4 vec4Albedo = texture(s2DDiffuseTexture, vec2FragmentUV);
	vec4 vec4Lighting = texture(s2DLightingTexture, vec2FragmentUV);

	// Assign a flat colour to all fragments
	vec4FragmentColour.xyz = vec4Albedo.xyz * 0.1;
	// Add on the lighting as required to each fragment
	// As the lighting is volume based & operates with the stencil buffer, areas that contain no information will 
	// store a (0,0,0,0) colour which will have no effect on the lighting.
	vec4FragmentColour.xyz += vec4Albedo.xyz * vec4Lighting.xyz;

	// Assign the output colour
	vec4FragmentColour = vec4(vec4FragmentColour.xyz, 1.0f);
}
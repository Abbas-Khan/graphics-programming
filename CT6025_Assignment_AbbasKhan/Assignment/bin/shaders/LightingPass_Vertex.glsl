///////////////////////////////////////////////////////////////////
// Name: LightingPass_Vertex.glsl
// Author: Abbas Khan
// Bio: Vertex shader for the lighting pass
///////////////////////////////////////////////////////////////////
#version 150

// Pass in variables for the Position (Vector 4) & UV (Vector 2)
in vec4 vec4Position;
in vec2 vec2UV;

// Output a vector 2 for the UV
out vec2 vec2UVoutput;

// Main Function
void main() 
{ 
	// Assign the UV value to the passed in uv
	vec2UVoutput = vec2UV;
	// Assign the GL Position to the passed in position
	gl_Position = vec4Position;
}
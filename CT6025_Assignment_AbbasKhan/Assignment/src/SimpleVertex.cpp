///////////////////////////////////////////////////////////////////
// Name: SimpleVertex.cpp
// Author: Abbas Khan
// Bio: Class used to hold the base tech behind a simple vertex
///////////////////////////////////////////////////////////////////

// Required includes
#include "SimpleVertex.h"

// Blank Constructor
SimpleVertex::SimpleVertex()
{
	// Assign default values to new simple vertex objects

	// Set position to 0,0,0
	vec4Position = glm::vec4(0, 0, 0, 1);
	// Set UV to 0,0
	vec2UV = glm::vec2(0, 0);
}

// Constructor with arguemnets to specify position & UV
SimpleVertex::SimpleVertex(const glm::vec4& a_vec4Position, const glm::vec2 a_vec2UV)
{
	// Assign the values passed in accordingly
	vec4Position = a_vec4Position;
	vec2UV = a_vec2UV;
}

// Constructor with arguemnets to specify position & UV through a simple vertex
SimpleVertex::SimpleVertex(const SimpleVertex& a_xSimpleVertex)
{
	// Assign the values passed in accordingly through the simple vertex object
	vec4Position = a_xSimpleVertex.vec4Position;
	vec2UV = a_xSimpleVertex.vec2UV;
}

// Destructor
SimpleVertex::~SimpleVertex()
{
}

// Comparison Operator overload for testing equality
bool SimpleVertex::operator == (const SimpleVertex& a_xSimpleVertex_RHS) const
{
	return memcmp(this, &a_xSimpleVertex_RHS, sizeof(SimpleVertex)) == 0;
}

// Comparison Operator overload for testing less than operation
bool SimpleVertex::operator < (const SimpleVertex& a_xSimpleVertex_RHS) const
{
	return memcmp(this, &a_xSimpleVertex_RHS, sizeof(SimpleVertex)) < 0;
}
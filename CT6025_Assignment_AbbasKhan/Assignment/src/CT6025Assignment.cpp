///////////////////////////////////////////////////////////////////
// Name: CT6025Assignment.cpp
// Author: Abbas Khan
// Bio: Class used as the main hub of the application
///////////////////////////////////////////////////////////////////

// Required includes
#include "CT6025Assignment.h"

// Constructor
CT6025Assignment::CT6025Assignment()
{
}

// Destructor
CT6025Assignment::~CT6025Assignment()
{
}

// Virtual OnCreate boolean function overridden from application class
bool CT6025Assignment::onCreate()
{
	// initialise the Gizmos helper class
	Gizmos::create();

	#pragma region Geometry Pass Setup
	// Assign the shaders to be used for this draw pass
	uiGeometryVertexShader = Utility::loadShader("./shaders/GeometryPass_Vertex.glsl", GL_VERTEX_SHADER);
	uiGeometryFragmentShader = Utility::loadShader("./shaders/GeometryPass_Fragment.glsl", GL_FRAGMENT_SHADER);
	// Define the input and output varialbes
	const char* szfboInputs[] = { "vec4Position", "vec4Normal", "vec2UV"};
	const char* szfboOutputs[] = { "vec4RTDiffuse", "vec4RTNormal", "vec4RTPosition"};
	// Create the program, bind the shaders & setup the input/outputs
	uiGeometryProgramID = Utility::createProgram(uiGeometryVertexShader, 0, 0, 0, uiGeometryFragmentShader, 3, szfboInputs, 3, szfboOutputs);
	#pragma endregion

	#pragma region Light Pass Setup
	// Assign the shaders to be used for this draw pass
	uiLightingVertexShader = Utility::loadShader("./shaders/LightingPass_Vertex.glsl", GL_VERTEX_SHADER);
	uiLightingFragmentShader = Utility::loadShader("./shaders/LightingPass_Fragment.glsl", GL_FRAGMENT_SHADER);
	// Define the input and output varialbes
	const char* szlightingInputs[] = { "vec4Position", "vec2UV" };
	const char* szlightingOutputs[] = { "vec4RTLighting" };
	// Create the program, bind the shaders & setup the input/outputs
	uiLightingProgramID = Utility::createProgram(uiLightingVertexShader, 0, 0, 0, uiLightingFragmentShader, 2, szlightingInputs, 1, szlightingOutputs);
	#pragma endregion

	#pragma region Composite Pass
	// Assign the shaders to be used for this draw pass
	uiCompositeVertexShader = Utility::loadShader("./shaders/CompositePass_Vertex.glsl", GL_VERTEX_SHADER);
	uiCompositeFragmentShader = Utility::loadShader("./shaders/CompositePass_Fragment.glsl", GL_FRAGMENT_SHADER);
	// Define the input and output varialbes
	const char* szInputs[] = { "vec4Position", "vec2UV" };
	const char* szOutputs[] = { "vec4FragmentColour" };
	// Create the program, bind the shaders & setup the input/outputs
	uiCompositeProgramID = Utility::createProgram(uiCompositeVertexShader, 0, 0, 0, uiCompositeFragmentShader, 2, szInputs, 1, szOutputs);
	#pragma endregion

	// Genorate the VBO, IBO and VAO
	glGenBuffers(2, uiVBO);
	glGenBuffers(2, uiIBO);
	glGenVertexArrays(2, uiVAO);

	#pragma region Geometry Pass VAO, VBO & IBO
	// Bind the VAO, IBO & VBO for setup
	glBindVertexArray(uiVAO[0]);
	glBindBuffer(GL_ARRAY_BUFFER, uiVBO[0]);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, uiIBO[0]);

	// Data filled in during mesh rendering loop (See Draw Function - Line 287+)

	// Setup the attribute locations that will be used for the shaders
	glEnableVertexAttribArray(0); // Position
	glEnableVertexAttribArray(1); // Normal
	glEnableVertexAttribArray(2); // UV

	// Tell the shaders where the information will be in the buffers
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(FBXVertex), ((char *)0) + FBXVertex::PositionOffset);
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_TRUE, sizeof(FBXVertex), ((char *)0) + FBXVertex::NormalOffset);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_TRUE, sizeof(FBXVertex), ((char *)0) + FBXVertex::TexCoord1Offset);
	#pragma endregion

	#pragma region Lighting & Composite Pass VAO, VBO & IBO
	// Bind the VAO, IBO & VBO for setup
	glBindVertexArray(uiVAO[1]);
	glBindBuffer(GL_ARRAY_BUFFER, uiVBO[1]);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, uiIBO[1]);

	// Simple Vertex setup to implement the full screen attributes 
	SimpleVertex vertexData[4];
	vertexData[0] = SimpleVertex(glm::vec4(-1.f,-1.f, 0.f, 1.f), glm::vec2(0.f, 0.f));
	vertexData[1] = SimpleVertex(glm::vec4( 1.f,-1.f, 0.f, 1.f), glm::vec2(1.f, 0.f));
	vertexData[2] = SimpleVertex(glm::vec4(-1.f, 1.f, 0.f, 1.f), glm::vec2(0.f, 1.f));
	vertexData[3] = SimpleVertex(glm::vec4( 1.f, 1.f, 0.f, 1.f), glm::vec2(1.f, 1.f));
	unsigned int elements[6] = { 0, 1, 2, 1, 3, 2 };

	// Fill data
	glBufferData(GL_ARRAY_BUFFER, 4 * sizeof(SimpleVertex), vertexData, GL_STATIC_DRAW);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, 6 * sizeof(unsigned int), elements, GL_STATIC_DRAW);

	// Setup the attribute locations that will be used for the shaders
	glEnableVertexAttribArray(0); // Position
	glEnableVertexAttribArray(1); // UV

	// Tell the shaders where the information will be in the buffers
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(SimpleVertex), ((char *)0) + SimpleVertex::PositionOffset);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_TRUE, sizeof(SimpleVertex), ((char *)0) + SimpleVertex::TexCoord1Offset);
	#pragma endregion

	#pragma region General FBO
	// Generate & Bind the general FBO
	glGenFramebuffers(1, &uiGeneralFBO);
	glBindFramebuffer(GL_FRAMEBUFFER, uiGeneralFBO);

	// Setup the diffuse texture to be created in the geometry pass for usuage in the composite pass
	glGenTextures(1, &uiDiffuseTexture);
	glBindTexture(GL_TEXTURE_2D, uiDiffuseTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, m_windowWidth, m_windowHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, uiDiffuseTexture, 0);
	glBindTexture(GL_TEXTURE_2D, 0);

	// Setup the normal texture to be created in the geometry pass for usuage in the composite pass
	glGenTextures(1, &uiNormalTexture);
	glBindTexture(GL_TEXTURE_2D, uiNormalTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, m_windowWidth, m_windowHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, uiNormalTexture, 0);
	glBindTexture(GL_TEXTURE_2D, 0);

	// Setup the position texture to be created in the geometry pass for usuage in the composite pass
	glGenTextures(1, &uiPositionTexture);
	glBindTexture(GL_TEXTURE_2D, uiPositionTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, m_windowWidth, m_windowHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, uiPositionTexture, 0);
	glBindTexture(GL_TEXTURE_2D, 0);

	// Setup the dpeth texture to be created in the geometry pass & to be used to form the linear depth
	glGenTextures(1, &uiDepthTexture);
	glBindTexture(GL_TEXTURE_2D, uiDepthTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32F, m_windowWidth, m_windowHeight, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, uiDepthTexture, 0);
	glBindTexture(GL_TEXTURE_2D, 0);

	// Setup the draw buffers with the corresponding colour attatchments
	GLenum drawBuffers[] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2};
	glDrawBuffers(3, drawBuffers);
	#pragma endregion

	#pragma region Screen FBO
	// Bind the screen buffer
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	// Setup the linear depth texture to be used in the composite pass
	glGenTextures(1, &uiLinearDepthTexture);
	glBindTexture(GL_TEXTURE_2D, uiLinearDepthTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, m_windowWidth, m_windowHeight, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glBindTexture(GL_TEXTURE_2D, 0);
	#pragma endregion

	#pragma region Lighting FBO
	// Generate & Bind the lighting FBO
	glGenFramebuffers(1, &uiLightingFBO);
	glBindFramebuffer(GL_FRAMEBUFFER, uiLightingFBO);

	// Setup the lighting texture to be created in the lighting pass for usuage in the composite pass
	glGenTextures(1, &uiLightingTexture);
	glBindTexture(GL_TEXTURE_2D, uiLightingTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, m_windowWidth, m_windowHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, uiLightingTexture, 0);
	glBindTexture(GL_TEXTURE_2D, 0);
	#pragma endregion

	// Set the clear colour, enable depth testing and backface culling
	glClearColor(0, 0, 0, 1.f);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);

	// Retrieve the output log
	xUI.xOutputLog = Application_Log::Get();

	// Safety null check
	if (xUI.xOutputLog != nullptr)
	{
		// Integer used to hold the maximum number of attatchments supported by the graphics card
		int iMaxAttachments = 0;
		// Retrieve the information
		glGetIntegerv(GL_MAX_COLOR_ATTACHMENTS, &iMaxAttachments);
		// Output it to the log
		xUI.xOutputLog->addLog(LOG_LEVEL::LOG_INFO, "Num FBO colour attachments supported: %i", iMaxAttachments);
	}

	#pragma region Setup Scene
	// Create & setup the matrix for the camera's position/orientation
	matCameraMatrix = glm::inverse(glm::lookAt(glm::vec3(12, 14, 6), glm::vec3(6, 12, 0), glm::vec3(0, 1, 0)));

	// Create a perspective projection matrix with a 90 degree field-of-view and widescreen aspect ratio
	mat4ProjectionMatrix = glm::perspective(glm::pi<float>() * 0.25f, (float)m_windowWidth / (float)m_windowHeight, 0.1f, 1000.0f);

	// Create & load in the environment model
	xEnvironmentModel = new FBXFile();
	xEnvironmentModel->load("./models/GraphicsEnvironment/Textures/Environment3.fbx", FBXFile::UNITS_INCH);

	// Setup the 3 default point light's within the scene (Light 1 - Red, Light 2 - Green & Light 3 - Blue)
	xPointLights[0] = PointLight(glm::vec4(6.9f, 11.8f, -2, 1.f), glm::vec4(1.0f, 0.0f, 0.0f, 1.f), glm::vec4(0.2f, 0.2f, 0.2f, 1.f),  glm::vec4(1.0f, 0.0f, 0.0f, 1.f), 10, 1);
	xPointLights[1] = PointLight(glm::vec4(-6.9f, 11.8f, -2, 1.f), glm::vec4(0.0f, 1.0f, 0.0f, 1.f), glm::vec4(0.2f, 0.2f, 0.2f, 1.f), glm::vec4(0.0f, 1.0f, 0.0f, 1.f), 10, 1);
	xPointLights[2] = PointLight(glm::vec4(0.f, 11.8f, -6.9f, 1.f), glm::vec4(0.0f, 0.0f, 1.0f, 1.f), glm::vec4(0.2f, 0.2f, 0.2f, 1.f), glm::vec4(0.0f, 0.0f, 1.0f, 1.f), 10, 1);
	// Set the amount of active lights
	uiAmountOfActiveLights = 3;
	#pragma endregion

	// Return true to indicate there was no errors in setup
	return true;
}

// Virtual Update function overridden from application class
void CT6025Assignment::Update(float a_fDeltaTime)
{
	// Allow for free camera movement
	Utility::freeMovement(matCameraMatrix, a_fDeltaTime, 10);
	
	// Clear the gizmos drawn in the previous frame
	Gizmos::clear();

	// Loop through all the lights
	for (unsigned int i = 0; i < uiAmountOfActiveLights; i++)
	{
		// Draw the gizmos to visualise the light volumes
		xPointLights[i].VisualiseLight(xPointLights[i], xUI.bShowWireFrame);
	}

	// Generate the UI for the frame buffers & the options menu
	xUI.GenerateFrameBufferUI(uiLightingTexture, uiDiffuseTexture, uiNormalTexture, uiPositionTexture, uiLinearDepthTexture);
	xUI.GenerateOptionsUI(xBGMAudio.bBGMDiscoMode, xBGMAudio.bFMODPlayedBGM, uiAmountOfActiveLights, xPointLights);

	// Update the audio & animation sequence systems
	xBGMAudio.AudioUpdate();
	xAnimation.AnimationSequenceUpdate(a_fDeltaTime, xBGMAudio.bBGMDiscoMode, xPointLights, uiAmountOfActiveLights, matCameraMatrix, mat4ProjectionMatrix);
}

// Virtual Draw function overridden from application class
void CT6025Assignment::Draw()
{
	#pragma region Geometry Pass
	// Bind the general buffer for writing to
	glBindFramebuffer(GL_FRAMEBUFFER, uiGeneralFBO);

	// Set the clear colour and clear the depth / colour buffer areas
	glClearColor(0, 0, 0, 1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Get the view matrix from the world-space camera matrix
	mat4ViewMatrix = glm::inverse(matCameraMatrix);

	// Update and draw the gizmos
	Gizmos::draw(mat4ViewMatrix, mat4ProjectionMatrix);

	// Bind the geometry pass shader program
	glUseProgram(uiGeometryProgramID);
	// Bind the corresponding VAO
	glBindVertexArray(uiVAO[0]);

	// Get the uniform location for the mat4ProjectionView
	unsigned int uiProjectionViewMatrixUniform = glGetUniformLocation(uiGeometryProgramID, "mat4ProjectionView");
	// Fill it with the data
	glUniformMatrix4fv(uiProjectionViewMatrixUniform, 1, false, glm::value_ptr(mat4ProjectionMatrix * mat4ViewMatrix));

	// For each mesh in the model
	for (unsigned int i = 0; i < xEnvironmentModel->getMeshCount(); ++i)
	{
		// Get the current mesh
		xMeshElement = xEnvironmentModel->getMeshByIndex(i);
		
		// Extract it's global transform information
		mat4ModelMatrix = xMeshElement->m_globalTransform;
		// Get the uniform location for the mat4Model
		unsigned int modelUniform = glGetUniformLocation(uiGeometryProgramID, "mat4Model");
		// Fill it with the data
		glUniformMatrix4fv(modelUniform, 1, false, glm::value_ptr(mat4ModelMatrix));

		// Get the normal matrix information
		mat4ModelNormalMatrix = glm::transpose(glm::inverse(mat4ModelMatrix));
		// Get the uniform location for the mat4NormalMatrix
		unsigned int uiNormalMatrixUniform = glGetUniformLocation(uiGeometryProgramID, "mat4NormalMatrix");
		// Fill it with the data
		glUniformMatrix4fv(uiNormalMatrixUniform, 1, false, glm::value_ptr(mat4ModelNormalMatrix));

		// Get the uniform location for s2DDiffuseTexture
		int iTextureUniformID = glGetUniformLocation(uiGeometryProgramID, "s2DDiffuseTexture");
		// Bind the texture
		glUniform1i(iTextureUniformID, 0);

		// Set the active texture
		glActiveTexture(GL_TEXTURE0);
		// Bind the loaded texture
		glBindTexture(GL_TEXTURE_2D, xMeshElement->m_material->textureIDs[FBXMaterial::DiffuseTexture]);

		// Send the vertex data to the VBO
		glBindBuffer(GL_ARRAY_BUFFER, uiVBO[0]);
		glBufferData(GL_ARRAY_BUFFER, xMeshElement->m_vertices.size() * sizeof(FBXVertex), xMeshElement->m_vertices.data(), GL_STATIC_DRAW);

		// send the index data to the IBO
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, uiIBO[0]);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, xMeshElement->m_indices.size() * sizeof(unsigned int), xMeshElement->m_indices.data(), GL_STATIC_DRAW);

		// Draw the model
		glDrawElements(GL_TRIANGLES, (GLsizei) xMeshElement->m_indices.size(), GL_UNSIGNED_INT, 0);
	}

	#pragma region Create linear depth texture
	// Bind the default VAO & Program
	glBindVertexArray(0);
	glUseProgram(0);
	// Get the recently created depth buffer texture from the FBO and linearise it
	glBindTexture(GL_TEXTURE_2D, uiDepthTexture);
	// Create a float array to store the depth texture
	GLfloat* xPixels = new GLfloat[m_windowWidth*m_windowHeight];
	// Get the depth texture data from the gpu
	glGetTexImage(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, GL_FLOAT, xPixels);

	// Float variables for the near & far planes in the z axis
	float fZNear = 0.1f;
	float fZFar = 100.f;

	// Loop through each pixel
	for (unsigned int i = 0; i < m_windowWidth * m_windowHeight; ++i)
	{
		// Get the pixel at the location
		float fDepthSample = xPixels[i];
		// Linerise each pixel value
		float fZLinear = (2.0f * fZNear) / (fZFar + fZNear - fDepthSample * (fZFar - fZNear));
		xPixels[i] = fZLinear;
	}

	// Generate the linear depth texture
	glBindTexture(GL_TEXTURE_2D, uiLinearDepthTexture);
	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, m_windowWidth, m_windowHeight, GL_DEPTH_COMPONENT, GL_FLOAT, xPixels);
	glGenerateMipmap(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, 0);

	// Delete the pixels
	delete[] xPixels;
	#pragma endregion

	#pragma endregion

	#pragma region Lighting Pass
	// Bind the lighting buffer for writing to
	glBindFramebuffer(GL_FRAMEBUFFER, uiLightingFBO);

	// Perform tee stencil pass
	DoStencilPass();

	// Bind the lighting pass shader program
	glUseProgram(uiLightingProgramID);

	// Bind the corresponding VAO, VBO & IBO
	glBindVertexArray(uiVAO[1]);
	glBindBuffer(GL_ARRAY_BUFFER, uiVBO[1]);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, uiIBO[1]);

	// Get the uniform location for the s2DNormalTexture
	unsigned int uiTextureLightingUniformID = glGetUniformLocation(uiLightingProgramID, "s2DNormalTexture");
	// Bind and activate the texture
	glUniform1i(uiTextureLightingUniformID, 1);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, uiNormalTexture);

	// Get the uniform location for the s2DPositionTexture
	uiTextureLightingUniformID = glGetUniformLocation(uiLightingProgramID, "s2DPositionTexture");
	// Bind and activate the texture
	glUniform1i(uiTextureLightingUniformID, 2);
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, uiPositionTexture);

	// Extract the camera position from the matrix
	glm::vec3 vec3CameraPosition = matCameraMatrix[3].xyz;
	// Get the uniform location for the vec3CameraPosition
	unsigned int uiCameraPositionLightingUniform = glGetUniformLocation(uiLightingProgramID, "vec3CameraPosition");
	// Fill it with the data
	glUniform3fv(uiCameraPositionLightingUniform, 1, glm::value_ptr(vec3CameraPosition));

	// Get the uniform location for the iAmountOfActiveLights
	unsigned int uiLightAmountUniform = glGetUniformLocation(uiLightingProgramID, "iAmountOfActiveLights");
	// Fill it with the data
	glUniform1i(uiLightAmountUniform, uiAmountOfActiveLights);

	// Loop therough all the active lights & pass through all the light information (Position, Colour, Radius & Intensity)
	for (unsigned int i = 0; i < uiAmountOfActiveLights; i++)
	{
		unsigned int uiLightPositionUniform = glGetUniformLocation(uiLightingProgramID, ("xLights[" + std::to_string(i) + "].vec3LightPosition").c_str());
		glUniform3fv(uiLightPositionUniform, 1, glm::value_ptr(xPointLights[i].vec4LightPosition));

		unsigned int uiLightDiffuseColourUniform = glGetUniformLocation(uiLightingProgramID, ("xLights[" + std::to_string(i) + "].vec3LightColour_Diffuse").c_str());
		glUniform3fv(uiLightDiffuseColourUniform, 1, glm::value_ptr(xPointLights[i].vec4LightColour_Diffuse));

		unsigned int uiLightSpecularUniform = glGetUniformLocation(uiLightingProgramID, ("xLights[" + std::to_string(i) + "].vec3LightColour_Specular").c_str());
		glUniform3fv(uiLightSpecularUniform, 1, glm::value_ptr(xPointLights[i].vec4LightColour_Specular));

		unsigned int uiLightAmbientUniform = glGetUniformLocation(uiLightingProgramID, ("xLights[" + std::to_string(i) + "].vec3LightColour_Ambient").c_str());
		glUniform3fv(uiLightAmbientUniform, 1, glm::value_ptr(xPointLights[i].vec4LightColour_Ambient));

		unsigned int uiLightRadiusUniform = glGetUniformLocation(uiLightingProgramID, ("xLights[" + std::to_string(i) + "].fLightRadius").c_str());
		glUniform1f(uiLightRadiusUniform, xPointLights[i].fLightRadius);	
	
		unsigned int uiLightIntensityUniform = glGetUniformLocation(uiLightingProgramID, ("xLights[" + std::to_string(i) + "].fLightIntensity").c_str());
		glUniform1f(uiLightIntensityUniform, xPointLights[i].fLightIntensity);
	}

	// Create a vector 2 to store the render target size of the window
	glm::vec2 vec2RenderTargetSize = glm::vec2(m_windowWidth, m_windowHeight);
	// Get the uniform location for the vec2RenderTargetSize
	unsigned int uiRenderTargetSizeLightingUniform = glGetUniformLocation(uiLightingProgramID, "vec2RenderTargetSize");
	// Fill it with the data
	glUniform2fv(uiRenderTargetSizeLightingUniform, 1, glm::value_ptr(vec2RenderTargetSize));

	// Draw the elements
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
	// Finished using stencil
	glDisable(GL_STENCIL_TEST);
	#pragma endregion

	#pragma region Composite Pass
	// Bind the framebuffer for rendering
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	// Set the clear colour and clear the depth / colour buffer areas
	glClearColor(0, 0, 0, 1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Bind the composite pass shader program
	glUseProgram(uiCompositeProgramID);

	// Bind the corresponding VAO, VBO & IBO
	glBindVertexArray(uiVAO[1]);
	glBindBuffer(GL_ARRAY_BUFFER, uiVBO[1]);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, uiIBO[1]);

	// Get the uniform location for the s2DDiffuseTexture
	unsigned int uiTextureCompositeUniform = glGetUniformLocation(uiCompositeProgramID, "s2DDiffuseTexture");
	// Bind and activate the texture
	glUniform1i(uiTextureCompositeUniform, 0);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, uiDiffuseTexture);

	// Get the uniform location for the s2DLightingTexture
	uiTextureCompositeUniform = glGetUniformLocation(uiCompositeProgramID, "s2DLightingTexture");
	// Bind and activate the texture
	glUniform1i(uiTextureCompositeUniform, 1);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, uiLightingTexture);

	// Get the uniform location for the vec2RenderTargetSize
	unsigned int uiRenderTargetSizeCompositeUniform = glGetUniformLocation(uiCompositeProgramID, "vec2RenderTargetSize");
	// Fill it with the data
	glUniform2fv(uiRenderTargetSizeCompositeUniform, 1, glm::value_ptr(vec2RenderTargetSize));

	// Draw the elements
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
	#pragma endregion
}

// Virtual Destroy function overridden from application class
void CT6025Assignment::Destroy()
{
	// Unload & destroy the model file to free the occupied memory
	xEnvironmentModel->unload();
	delete xEnvironmentModel;

	// Delete all the generated textures to free up the memory
	glDeleteTextures(1, &uiDiffuseTexture);
	glDeleteTextures(1, &uiNormalTexture);
	glDeleteTextures(1, &uiPositionTexture);
	glDeleteTextures(1, &uiDepthTexture);
	glDeleteTextures(1, &uiLinearDepthTexture);
	glDeleteTextures(1, &uiLightingTexture);

	// Delete the buffers used for the VAO, IBO & VBO
	glDeleteBuffers(2, uiVBO);
	glDeleteBuffers(2, uiIBO);
	glDeleteVertexArrays(2, uiVAO);

	// Delete the frame buffer objects
	glDeleteFramebuffers(1, &uiGeneralFBO);
	glDeleteFramebuffers(1, &uiLightingFBO);

	// Delete the components of the composite draw phase (Program, Fragment & Vertex Shaders)
	glDeleteProgram(uiCompositeProgramID);
	glDeleteShader(uiCompositeFragmentShader);
	glDeleteShader(uiCompositeVertexShader);

	// Delete the components of the lighting draw phase (Program, Fragment & Vertex Shaders)
	glDeleteProgram(uiLightingProgramID);
	glDeleteShader(uiLightingFragmentShader);
	glDeleteShader(uiLightingVertexShader);

	// Delete the components of the geometry draw phase (Program, Fragment & Vertex Shaders)
	glDeleteProgram(uiGeometryProgramID);
	glDeleteShader(uiGeometryFragmentShader);
	glDeleteShader(uiGeometryVertexShader);

	// Destroy the gizmos
	Gizmos::destroy();
}

// Function used to perform the stencil pass
void CT6025Assignment::DoStencilPass()
{
	// Initialize stencil clear value
	glClearStencil(0);

	// Clear colour and stencil buffer
	glClear(GL_COLOR_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

	// Disable rendering to the colour buffer
	glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);

	// Start using the stencil
	glEnable(GL_STENCIL_TEST);

	// Place a 1 where rendered
	glStencilFunc(GL_ALWAYS, 1, 1);

	// Replace where rendered
	glStencilOp(GL_REPLACE, GL_REPLACE, GL_REPLACE);

	// Get the view matrix from the world-space camera matrix
	mat4ViewMatrix = glm::inverse(matCameraMatrix);

	// Update and draw the gizmos
	Gizmos::draw(mat4ViewMatrix, mat4ProjectionMatrix);

	// Reenable colour
	glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);

	// Where a 1 was rendered
	glStencilFunc(GL_EQUAL, 1, 1);

	// Keep the pixel
	glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
}
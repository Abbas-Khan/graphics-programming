///////////////////////////////////////////////////////////////////
// Name: main.cpp
// Author: Abbas Khan
// Bio: Source file that serves as the entry point for the application
///////////////////////////////////////////////////////////////////

// Include for the application header
#include "CT6025Assignment.h"

// Main function used as an application entry ppint
int main()
{
	// Variable used to hold a refeence to the application
	CT6025Assignment* xApplication = new CT6025Assignment();
	// Run and create a new window with the specified dimensions, title amd maximised status
	xApplication->run("CT6025_Assignment_Application", DEFAULT_SCREENWIDTH, DEFAULT_SCREENHEIGHT, false);
	// Upon application run completion, delete the reference
	delete xApplication;
	// Return 0 to indicate no issues
	return 0;
}
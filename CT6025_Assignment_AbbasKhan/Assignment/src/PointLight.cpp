///////////////////////////////////////////////////////////////////
// Name: PointLight.cpp
// Author: Abbas Khan
// Bio: Class used to hold the base tech behind a point light
///////////////////////////////////////////////////////////////////

// Required includes
#include "PointLight.h"

// Blank Constructor
PointLight::PointLight()
{
	// Assign default values to new point lights

	// Position of 0,0,0
	vec4LightPosition = glm::vec4(0.f, 0.f, 0.f, 1.f);

	// White Light Colour
	vec4LightColour_Diffuse = glm::vec4(0.1f, 0.1f, 0.1f, 1.f);
	vec4LightColour_Specular = glm::vec4(0.1f, 0.1f, 0.1f, 1.f);
	vec4LightColour_Ambient = glm::vec4(1.0f, 1.0f, 1.0f, 1.f);

	// Intensity of 1 & Radius of 5
	fLightIntensity = 1.0f;
	fLightRadius = 5.0f;
}

// Constructor with arguemnets to specify position, light colour, radius & intensity
PointLight::PointLight(glm::vec4 a_vec4LightPosition, glm::vec4 a_vec4LightColour_Diffuse, glm::vec4 a_vec4LightColour_Specular, glm::vec4 a_vec4LightColour_Ambient, float a_fLightRadius, float a_fLightIntensity)
{
	// Assign values accordingly from passed in variables
	vec4LightPosition = a_vec4LightPosition;
	vec4LightColour_Diffuse = a_vec4LightColour_Diffuse;
	vec4LightColour_Specular = a_vec4LightColour_Specular;
	vec4LightColour_Ambient = a_vec4LightColour_Ambient;
	fLightIntensity = a_fLightIntensity;
	fLightRadius = a_fLightRadius;
}

// Destructor
PointLight::~PointLight()
{
}

// Function used to display the volume for a light with arguemnets for the light & fill status
void PointLight::VisualiseLight(PointLight & a_xLight, bool a_bFilledIn)
{
	// Create a sphere using the properties from the light & fill it in depending on the boolean value
	Gizmos::addSphere(a_xLight.vec4LightPosition.xyz, 20, 20, a_xLight.fLightRadius, a_xLight.vec4LightColour_Diffuse, a_bFilledIn);
}
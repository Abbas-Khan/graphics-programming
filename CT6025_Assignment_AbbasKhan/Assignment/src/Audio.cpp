///////////////////////////////////////////////////////////////////
// Name: Audio.cpp
// Author: Abbas Khan
// Bio: Class used to hold the base tech behind the audio
///////////////////////////////////////////////////////////////////

// Required includes
#include "Audio.h"

// Blank Constructor
Audio::Audio()
{
}

// Destructor
Audio::~Audio()
{
}

// Function used to update the audio
void Audio::AudioUpdate()
{
	// If the background music is not playing
	if (bFMODPlayedBGM == false)
	{
		// Release call to prevent any overlap
		xFMODSystemBGM->release();
		// Create and setup the system
		FMOD::System_Create(&xFMODSystemBGM);
		xFMODSystemBGM->init(32, FMOD_INIT_NORMAL, nullptr);
		// If disco mode is active
		if (bBGMDiscoMode == true)
		{
			// Setup the sounds to play the disco audio
			xFMODSystemBGM->createSound("./audio/StringedDisco.mp3", FMOD_DEFAULT, 0, &xFMODSoundBGMDisco);
			xFMODSoundBGM->setMode(FMOD_LOOP_NORMAL);
			xFMODSoundBGMDisco->setMode(FMOD_LOOP_NORMAL);
			// Play it
			xFMODSystemBGM->playSound(xFMODSoundBGMDisco, 0, false, &xFMODChannelBGM);
		}
		else
		{
			// Otherwise play the relaxing auido
			xFMODSystemBGM->createSound("./audio/MidnightTale.mp3", FMOD_DEFAULT, 0, &xFMODSoundBGM);
			xFMODSoundBGM->setMode(FMOD_LOOP_NORMAL);
			xFMODSoundBGMDisco->setMode(FMOD_LOOP_NORMAL);
			// Play it
			xFMODSystemBGM->playSound(xFMODSoundBGM, 0, false, &xFMODChannelBGM);
		}
		// Set played to true
		bFMODPlayedBGM = true;
	}
}
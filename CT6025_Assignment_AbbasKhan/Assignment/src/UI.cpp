///////////////////////////////////////////////////////////////////
// Name: UI.cpp
// Author: Abbas Khan
// Bio: Class used to hold the tech behind the UI
///////////////////////////////////////////////////////////////////

// Required includes
#include "UI.h"

// Blank Constructor
UI::UI()
{
}

// Destructor
UI::~UI()
{
}

// Function used to generate the options IMGUI interface
void UI::GenerateOptionsUI(bool& a_bDiscoMode, bool& a_bAudioPlaying, unsigned int& a_uiAmountOfActiveLights, PointLight a_xPointlights[])
{
	// Set the window dimensions, title & position
	ImGui::SetNextWindowPos(ImVec2(DEFAULT_SCREENWIDTH - DEFAULT_SCREENWIDTH * 0.275f, DEFAULT_SCREENHEIGHT * 0.01f));
	ImGui::SetNextWindowSize(ImVec2(DEFAULT_SCREENWIDTH*0.265f, DEFAULT_SCREENHEIGHT* 0.98f));
	ImGui::Begin("Menu");
	ImGui::Separator();

	#pragma region Information UI
	// Information section
	ImGui::TextColored(ImVec4(0, 1, 0.8f, 1), "Information");
	ImGui::Separator();
	ImGui::TextWrapped("Welcome to Abbas Khan's Deferred Rendering Environment Simulation for CT6025 - Graphics Programming");
	ImGui::Separator();
	ImGui::TextWrapped("Below you will find multiple sections which can be modified to achieve a different outcome for the how the scene is rendered.");
	ImGui::Separator();
	#pragma endregion

	if (a_bDiscoMode == false)
	{
		// Light Settings
		ImGui::TextColored(ImVec4(0, 1, 0.8f, 1), "Light Settings");
		ImGui::Separator();

		#pragma region General Light Settings
		ImGui::TextWrapped("Amount of Lights:");
		ImGui::SameLine();
		ImGui::Text("%d", a_uiAmountOfActiveLights);

		if (ImGui::Button("Decrease Lights"))
		{
			if (a_uiAmountOfActiveLights > 0)
			{
				// Decrease the amount of lights
				a_uiAmountOfActiveLights = a_uiAmountOfActiveLights - 1;
			}
		}
		ImGui::SameLine(0.0f, ImGui::GetStyle().ItemInnerSpacing.x);
		if (ImGui::Button("Increase Lights"))
		{
			if (a_uiAmountOfActiveLights < 32)
			{
				// Increase the amount of lights
				a_uiAmountOfActiveLights = a_uiAmountOfActiveLights + 1;
				// Populate new light with random properties
				a_xPointlights[a_uiAmountOfActiveLights - 1].vec4LightPosition = glm::vec4(0, 10, 0, 1);
				a_xPointlights[a_uiAmountOfActiveLights - 1].vec4LightColour_Diffuse = glm::vec4(1, 1, 1, 1);
				a_xPointlights[a_uiAmountOfActiveLights - 1].vec4LightColour_Ambient = glm::vec4(1, 1, 1, 1);
				a_xPointlights[a_uiAmountOfActiveLights - 1].vec4LightColour_Specular = glm::vec4(1, 1, 1, 1);
				a_xPointlights[a_uiAmountOfActiveLights - 1].fLightRadius = 3;
				a_xPointlights[a_uiAmountOfActiveLights - 1].fLightIntensity = 1;
			}
		}
		#pragma endregion
		ImGui::Separator();

		#pragma region Light Selection Settings
		ImGui::TextWrapped("Selected Light:");
		ImGui::SameLine();
		ImGui::Text("%d", uiSelectedLight);

		if (ImGui::Button("Selection -"))
		{
			if (uiSelectedLight > 0)
			{
				// Decrease the selected light
				uiSelectedLight = uiSelectedLight - 1;
			}
		}
		ImGui::SameLine(0.0f, ImGui::GetStyle().ItemInnerSpacing.x);
		if (ImGui::Button("Selection +"))
		{
			if (uiSelectedLight < a_uiAmountOfActiveLights - 1)
			{
				// Increase the selected light
				uiSelectedLight = uiSelectedLight + 1;
			}
		}
		#pragma endregion
		ImGui::Separator();

		#pragma region Individual Light Settings
		// Light options - Position, Diffuse, Ambient, Specular, Radius, Intensity
		// Get the properties
		fLightPositions[0] = { a_xPointlights[uiSelectedLight].vec4LightPosition.x };
		fLightPositions[1] = { a_xPointlights[uiSelectedLight].vec4LightPosition.y };
		fLightPositions[2] = { a_xPointlights[uiSelectedLight].vec4LightPosition.z };
		fLightDiffuse[0] = { a_xPointlights[uiSelectedLight].vec4LightColour_Diffuse.x };
		fLightDiffuse[1] = { a_xPointlights[uiSelectedLight].vec4LightColour_Diffuse.y };
		fLightDiffuse[2] = { a_xPointlights[uiSelectedLight].vec4LightColour_Diffuse.z };
		fLightAmbient[0] = { a_xPointlights[uiSelectedLight].vec4LightColour_Ambient.x };
		fLightAmbient[1] = { a_xPointlights[uiSelectedLight].vec4LightColour_Ambient.y };
		fLightAmbient[2] = { a_xPointlights[uiSelectedLight].vec4LightColour_Ambient.z };
		fLightSpecular[0] = { a_xPointlights[uiSelectedLight].vec4LightColour_Specular.x };
		fLightSpecular[1] = { a_xPointlights[uiSelectedLight].vec4LightColour_Specular.y };
		fLightSpecular[2] = { a_xPointlights[uiSelectedLight].vec4LightColour_Specular.z };
		fLightRadius = a_xPointlights[uiSelectedLight].fLightRadius;
		fLightIntensity = a_xPointlights[uiSelectedLight].fLightIntensity;
		// Modify Them
		ImGui::SliderFloat3("Position", fLightPositions, -30, 30);
		ImGui::ColorEdit3("Diffuse", fLightDiffuse);
		ImGui::ColorEdit3("Ambient", fLightAmbient);
		ImGui::ColorEdit3("Specular", fLightSpecular);
		ImGui::SliderFloat("Radius", &fLightRadius, 0, 40);
		ImGui::SliderFloat("Intensity", &fLightIntensity, 0, 5);
		// Apply to the selected light
		a_xPointlights[uiSelectedLight].vec4LightPosition = glm::vec4(fLightPositions[0], fLightPositions[1], fLightPositions[2], 1);
		a_xPointlights[uiSelectedLight].vec4LightColour_Diffuse = glm::vec4(fLightDiffuse[0], fLightDiffuse[1], fLightDiffuse[2], 1);
		a_xPointlights[uiSelectedLight].vec4LightColour_Ambient = glm::vec4(fLightAmbient[0], fLightAmbient[1], fLightAmbient[2], 1);
		a_xPointlights[uiSelectedLight].vec4LightColour_Specular = glm::vec4(fLightSpecular[0], fLightSpecular[1], fLightSpecular[2], 1);
		a_xPointlights[uiSelectedLight].fLightRadius = fLightRadius;
		a_xPointlights[uiSelectedLight].fLightIntensity = fLightIntensity;
		#pragma endregion
		ImGui::Separator();
	}

	#pragma region Animation Settings
	// Animation Settings
	ImGui::TextColored(ImVec4(0, 1, 0.8f, 1), "Animation Settings");
	ImGui::Separator();
	// Disco Lighting buttons
	if (a_bDiscoMode == true)
	{
		if (ImGui::Button("Disable Disco Mode"))
		{
			// Set the playback to false
			a_bAudioPlaying = false;
			// Disable disco mode
			a_bDiscoMode = false;
			// Reset the simulation
			a_xPointlights[0] = PointLight(glm::vec4(6.9f, 11.8f, -2, 1.f), glm::vec4(1.0f, 0.0f, 0.0f, 1.f), glm::vec4(0.2f, 0.2f, 0.2f, 1.f), glm::vec4(1.0f, 0.0f, 0.0f, 1.f), 10, 1);
			a_xPointlights[1] = PointLight(glm::vec4(-6.9f, 11.8f, -2, 1.f), glm::vec4(0.0f, 1.0f, 0.0f, 1.f), glm::vec4(0.2f, 0.2f, 0.2f, 1.f), glm::vec4(0.0f, 1.0f, 0.0f, 1.f), 10, 1);
			a_xPointlights[2] = PointLight(glm::vec4(0.f, 11.8f, -6.9f, 1.f), glm::vec4(0.0f, 0.0f, 1.0f, 1.f), glm::vec4(0.2f, 0.2f, 0.2f, 1.f), glm::vec4(0.0f, 0.0f, 1.0f, 1.f), 10, 1);
			a_uiAmountOfActiveLights = 3;
			uiSelectedLight = 0;
		}
	}
	else
	{
		if (ImGui::Button("Enable Animation Sequence"))
		{
			// Set the playback to false
			a_bAudioPlaying = false;
			// Enable disco mode
			a_bDiscoMode = true;
		}
	}
	ImGui::Separator();
	#pragma endregion

	#pragma region Credits UI
	// Credits section
	ImGui::TextColored(ImVec4(0, 1, 0.8f, 1), "Credits");
	ImGui::Separator();
	ImGui::TextWrapped("Midnight Tale - Kevin MacLeod (incompetech.com)");
	ImGui::TextWrapped("Stringed Disco - Kevin MacLeod (incompetech.com)");
	ImGui::TextWrapped("All of these sounds are licensed under CC 3.0 http://creativecommons.org/licenses/by/3.0/");
	ImGui::Separator();
	#pragma endregion

	#pragma region Miscellaneous Settings UI
	// General Setting Section
	ImGui::TextColored(ImVec4(0, 1, 0.8f, 1), "Miscellaneous Settings");
	ImGui::Separator();
	// Retrieve the output log
	xOutputLog = Application_Log::Get();

	// Checkbox for the output log
	ImGui::Checkbox("Fill Light Volumes", &bShowWireFrame);
	// Checkbox for the output log
	ImGui::Checkbox("Enable Output Log", &bShowOutputLog);
	ImGui::Separator();
	#pragma endregion

	// Retrieve the output log
	xOutputLog = Application_Log::Get();
	// If the output log is not null & show is true
	if (xOutputLog != nullptr && bShowOutputLog == true)
	{
		// Display the output log
		xOutputLog->showLog(&bShowOutputLog);
	}
	ImGui::End();
}

// Function used to generate the Frame Buffer IMGUI interface
void UI::GenerateFrameBufferUI(unsigned int& a_uiLightingTexture, unsigned int& a_uiDiffuseTexture, unsigned int& a_uiNormalTexture, unsigned int& a_uiPositionTexture, unsigned int& a_uiLinearDepthTexture)
{
	// Set the window dimensions, title & position
	ImGui::SetNextWindowPos(ImVec2(DEFAULT_SCREENWIDTH * 0.01f, DEFAULT_SCREENHEIGHT - DEFAULT_SCREENHEIGHT * 0.35f));
	ImGui::SetNextWindowSize(ImVec2(DEFAULT_SCREENWIDTH*0.265f, DEFAULT_SCREENHEIGHT* 0.34f));
	ImGui::Begin("Framebuffer");
	ImGui::BeginTabBar("FrameBuffer Textures");

	// Light Texture
	if (ImGui::BeginTabItem("Light"))
	{
		// Get the texture & display it on an image 
		xTextureID = (void *)(intptr_t)a_uiLightingTexture;
		ImGui::Image(xTextureID, ImVec2(DEFAULT_SCREENWIDTH*0.25f, DEFAULT_SCREENHEIGHT*0.25f), ImVec2(0, 1), ImVec2(1, 0));
		ImGui::EndTabItem();
	}
	// Colour Texture
	if (ImGui::BeginTabItem("Colour"))
	{
		// Get the texture & display it on an image 
		xTextureID = (void *)(intptr_t)a_uiDiffuseTexture;
		ImGui::Image(xTextureID, ImVec2(DEFAULT_SCREENWIDTH*0.25f, DEFAULT_SCREENHEIGHT*0.25f), ImVec2(0, 1), ImVec2(1, 0));
		ImGui::EndTabItem();
	}
	// Normal Texture
	if (ImGui::BeginTabItem("Normal"))
	{
		// Get the texture & display it on an image 
		xTextureID = (void *)(intptr_t)a_uiNormalTexture;
		ImGui::Image(xTextureID, ImVec2(DEFAULT_SCREENWIDTH*0.25f, DEFAULT_SCREENHEIGHT*0.25f), ImVec2(0, 1), ImVec2(1, 0));
		ImGui::EndTabItem();
	}
	// Position Texture
	if (ImGui::BeginTabItem("Pos"))
	{
		// Get the texture & display it on an image 
		xTextureID = (void *)(intptr_t)a_uiPositionTexture;
		ImGui::Image(xTextureID, ImVec2(DEFAULT_SCREENWIDTH*0.25f, DEFAULT_SCREENHEIGHT*0.25f), ImVec2(0, 1), ImVec2(1, 0));
		ImGui::EndTabItem();
	}
	// Depdth Texture
	if (ImGui::BeginTabItem("Depth"))
	{
		// Get the texture & display it on an image 
		xTextureID = (void *)(intptr_t)a_uiLinearDepthTexture;
		ImGui::Image(xTextureID, ImVec2(DEFAULT_SCREENWIDTH*0.25f, DEFAULT_SCREENHEIGHT*0.25f), ImVec2(0, 1), ImVec2(1, 0));
		ImGui::EndTabItem();
	}

	ImGui::EndTabBar();
	ImGui::End();
}
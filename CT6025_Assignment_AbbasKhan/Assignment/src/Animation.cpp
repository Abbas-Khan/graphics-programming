///////////////////////////////////////////////////////////////////
// Name: Animation.cpp
// Author: Abbas Khan
// Bio: Class used to hold the base tech behind the animation
///////////////////////////////////////////////////////////////////

// Required includes
#include "Animation.h"

// Constructor
Animation::Animation()
{
}

// Destructor
Animation::~Animation()
{
}

// Function used to update the animation sequence
void Animation::AnimationSequenceUpdate(float a_fDeltaTime, bool a_bDiscoMode, PointLight a_xPointLights[], unsigned int a_uiAmountOfActiveLights, glm::mat4 &a_mat4CameraMatrix, glm::mat4 &a_mat4ProjectionMatrix)
{
	// If the animation sequence is active
	if (a_bDiscoMode == true)
	{
		// Rotate the light's position
		float s = sinf(a_fDeltaTime * 3);
		float c = cosf(a_fDeltaTime * 3);
		vec4PositionOffset = a_xPointLights[0].vec4LightPosition;
		a_xPointLights[0].vec4LightPosition.x = vec4PositionOffset.x * c - vec4PositionOffset.z * s;
		a_xPointLights[0].vec4LightPosition.z = vec4PositionOffset.x * s + vec4PositionOffset.z * c;
		// Move the camera 
		a_mat4CameraMatrix = glm::inverse(glm::lookAt(glm::vec3(12, vec4PositionOffset.x + 25, 6), glm::vec3(6, 12, 0), glm::vec3(0, 1, 0)));
		// Create a perspective projection matrix with a 90 degree field-of-view and widescreen aspect ratio
		a_mat4ProjectionMatrix = glm::perspective(glm::pi<float>() * 0.25f, (float)DEFAULT_SCREENWIDTH / (float)DEFAULT_SCREENHEIGHT, 0.1f, 1000.0f);
		// Add a sphere as a disco ball
		Gizmos::addSphere(glm::vec3(7.1f, 11.4f, -1.7f), 10, 10, 1, glm::vec4(glm::linearRand(0.f, 1.f), glm::linearRand(0.f, 1.f), glm::linearRand(0.f, 1.f), 1), true);
		// Light colour Variation
		for (unsigned int i = 0; i < a_uiAmountOfActiveLights; i++)
		{
			a_xPointLights[i].vec4LightColour_Diffuse = glm::vec4(glm::linearRand(0.f, 1.f), glm::linearRand(0.f, 1.f), glm::linearRand(0.f, 1.f), 1);
			a_xPointLights[i].vec4LightColour_Ambient = a_xPointLights[i].vec4LightColour_Diffuse;
			a_xPointLights[i].vec4LightColour_Specular = a_xPointLights[i].vec4LightColour_Diffuse;
		}
	}
}
#ifndef _AUDIO_H
#define _AUDIO_H

///////////////////////////////////////////////////////////////////
// Name: Audio.h
// Author: Abbas Khan
// Bio: Class used to hold the base tech behind the audio
///////////////////////////////////////////////////////////////////

// Required includes
#include "fmod.hpp"

// Audio Class
class Audio
{
public:
	// Blank Constructor
	Audio();
	// Destructor
	~Audio();
	// Function used to update the audio
	void AudioUpdate();

	// BGM FMOD Variables
	FMOD::System* xFMODSystemBGM;
	FMOD::Sound* xFMODSoundBGM;
	FMOD::Sound* xFMODSoundBGMDisco;
	FMOD::Channel* xFMODChannelBGM;
	// Boolean used to determine if the audio is playing
	bool bFMODPlayedBGM;
	// Boolean used to determine if the disco mode is activated
	bool bBGMDiscoMode;
};

#endif // _AUDIO_H
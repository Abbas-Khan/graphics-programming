#ifndef _POINTLIGHT_H
#define _POINTLIGHT_H

///////////////////////////////////////////////////////////////////
// Name: PointLight.h
// Author: Abbas Khan
// Bio: Class used to hold the base tech behind a point light
///////////////////////////////////////////////////////////////////

// Required includes
#include "Gizmos.h"

// Point Light Class
class PointLight
{
public:
	// Blank Constructor
	PointLight();
	// Constructor with arguemnets to specify position, light colour, radius & intensity
	PointLight(glm::vec4 a_vec4LightPosition, glm::vec4 a_vec4LightColour_Diffuse, glm::vec4 a_vec4LightColour_Specular, glm::vec4 a_vec4LightColour_Ambient, float a_fLightRadius, float a_fLightIntensity);
	// Destructor
	~PointLight();
	// Function used to display the volume for a light with arguemnets for the light & fill status
	void PointLight::VisualiseLight(PointLight & a_xLight, bool a_bFilledIn);

	// Vector 4 to hold the light's position
	glm::vec4 vec4LightPosition;
	// Vector 4 to hold the light's diffuse colour
	glm::vec4 vec4LightColour_Diffuse;
	// Vector 4 to hold the light's specular colour
	glm::vec4 vec4LightColour_Specular;
	// Vector 4 to hold the light's ambient colour
	glm::vec4 vec4LightColour_Ambient;
	// Float to hold the light's intsnity
	float fLightIntensity;
	// Float to hold the light's radius
	float fLightRadius;
};

#endif // _POINTLIGHT_H
#ifndef _UI_H
#define _UI_H

///////////////////////////////////////////////////////////////////
// Name: UI.h
// Author: Abbas Khan
// Bio: Class used to hold the tech behind the UI
///////////////////////////////////////////////////////////////////

// Required includes
#include "imgui.h"
#include "Constants.h"
#include "glm/glm.hpp"
#include "Application_Log.h"
#include "PointLight.h"

// Audio Class
class UI
{
public:
	// Blank Constructor
	UI();
	// Destructor
	~UI();

	// Function used to generate the options IMGUI interface
	void UI::GenerateOptionsUI(bool& a_bDiscoMode, bool& a_bAudioPlaying, unsigned int& a_uiAmountOfActiveLights, PointLight a_xPointlights[]);
	// Function used to generate the Frame Buffer IMGUI interface
	void UI::GenerateFrameBufferUI(unsigned int& a_uiLightingTexture, unsigned int& a_uiDiffuseTexture, unsigned int& a_uiNormalTexture, unsigned int& a_uiPositionTexture, unsigned int& a_uiLinearDepthTexture);

	// Variables used to modify light properties through IMGUI
	float fLightPositions[3];
	float fLightDiffuse[3];
	float fLightAmbient[3];
	float fLightSpecular[3];
	float fLightRadius;
	float fLightIntensity;

	// Texture handle for the Frame Buffer Window Image
	ImTextureID xTextureID;

	// Variable to hold a refeence to the output log
	Application_Log* xOutputLog;
	// Boolean used to determine the display status of the output log
	bool bShowOutputLog;
	// Boolean used to determine the wireframe status
	bool bShowWireFrame;

	// Unsigned integer used to store the currently selected light for editing within the IMGUI editor
	unsigned int uiSelectedLight = 0;
};

#endif // _UI_H
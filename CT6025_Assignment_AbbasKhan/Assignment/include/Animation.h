#ifndef _ANIMATION_H
#define _ANIMATION_H

///////////////////////////////////////////////////////////////////
// Name: Animation.h
// Author: Abbas Khan
// Bio: Class used to hold the base tech behind the animation
///////////////////////////////////////////////////////////////////

// Required includes
#include "glm/ext.hpp"
#include "Gizmos.h"
#include "Constants.h"
#include "PointLight.h"

// Audio Class
class Animation
{
public:
	// Blank Constructor
	Animation();
	// Destructor
	~Animation();
	// Function used to update the animation sequence
	void Animation::AnimationSequenceUpdate(float a_fDeltaTime, bool a_bDiscoMode, PointLight a_xPointLights[], unsigned int a_uiAmountOfActiveLights, glm::mat4 &a_mat4CameraMatrix, glm::mat4 &a_mat4ProjectionMatrix);
	// Variable used to store the position offset for the animation sequence
	glm::vec4 vec4PositionOffset;
};

#endif // _ANIMATION_H
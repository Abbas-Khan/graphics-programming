#ifndef _CONSTANTS_H
#define _CONSTANTS_H

///////////////////////////////////////////////////////////////////
// Name: Constants.h
// Author: Abbas Khan
// Bio: Header file used to hold all the constants
///////////////////////////////////////////////////////////////////

// #defines for screen width and height
#define DEFAULT_SCREENWIDTH 1024
#define DEFAULT_SCREENHEIGHT 720

#endif //_CONSTANTS_H

#ifndef _CT6025ASSIGNMENT_H
#define _CT6025ASSIGNMENT_H

///////////////////////////////////////////////////////////////////
// Name: CT6025Assignment.h
// Author: Abbas Khan
// Bio: Class used as the main hub of the application
///////////////////////////////////////////////////////////////////

// Project Includes
#include "SimpleVertex.h"
#include "PointLight.h"
#include "Audio.h"
#include "UI.h"
#include "Animation.h"

// External Includes
#include "FBXFile.h"
#include <glad/glad.h>
#include <glm/ext.hpp>

// Framework Includes
#include "Application.h"
#include "Application_Log.h"
#include "Utilities.h"

// CT6025 Assignment Class that inherits from the Application class
class CT6025Assignment : public Application
{
public:
	// Constructor
	CT6025Assignment();
	// Destructor
	~CT6025Assignment();

	// Virtual OnCreate boolean function overridden from application class
	virtual bool onCreate();
	// Virtual Update function overridden from application class
	virtual void Update(float a_fDeltaTime);
	// Virtual Draw function overridden from application class
	virtual void Draw();
	// Virtual Destroy function overridden from application class
	virtual void Destroy();

	// Function used to perform the stencil pass
	void CT6025Assignment::DoStencilPass();

	// Matrix 4 variable used to store camera information
	glm::mat4 matCameraMatrix;
	// Matrix 4 variable used to store projection information
	glm::mat4 mat4ProjectionMatrix;
	// Matrix 4 used to store the view matrix
	glm::mat4 mat4ViewMatrix;

	// Unsigned integers used to hold a reference to the VAO, VBO & IBO (Only 2 needed because one for geometry draw & one for lighting. Composite pass shares with lighting)
	unsigned int uiVAO[2];
	unsigned int uiVBO[2];
	unsigned int uiIBO[2];

	// Unsigned integers used to reference the components for the geometry pass (Program ID, Vertex & Fragment Shader)
	unsigned int uiGeometryProgramID;
	unsigned int uiGeometryVertexShader;
	unsigned int uiGeometryFragmentShader;

	// Unsigned integers used to reference the components for the composite pass (Program ID, Vertex & Fragment Shader)
	unsigned int uiCompositeProgramID;
	unsigned int uiCompositeVertexShader;
	unsigned int uiCompositeFragmentShader;
	
	// Unsigned integers used to reference the components for the lighting pass (Program ID, Vertex & Fragment Shader)
	unsigned int uiLightingProgramID;
	unsigned int uiLightingVertexShader;
	unsigned int uiLightingFragmentShader;

	// Unsigned integers used for the FBO for the respective draw pass (The composite pass is drawn directly to the screen & not into an FBO)
	unsigned int uiGeneralFBO;
	unsigned int uiLightingFBO;

	// Unsigned integers to hold a reference to the textures used throughout the lifetime of the project (Diffuse, Normal, Position, Linear Depth, Depth & Lighting)
	unsigned int uiDiffuseTexture;
	unsigned int uiNormalTexture;
	unsigned int uiPositionTexture;
	unsigned int uiDepthTexture;
	unsigned int uiLinearDepthTexture;
	unsigned int uiLightingTexture;

	// Point Light array used to store all the lights within the scene
	PointLight xPointLights[32];
	// Unsigned integer used to hold the amount of active lights within the scene
	unsigned int uiAmountOfActiveLights;

	// Variable to hold a reference to the environemt fbx file
	FBXFile* xEnvironmentModel;
	// Matrix 4 for the model matrix
	glm::mat4 mat4ModelMatrix;
	// Matrix 4 for the model normal matrix
	glm::mat4 mat4ModelNormalMatrix;
	// Variable used to keep a reference to the current mesh element
	FBXMeshNode *xMeshElement;

	// Variable used to store the UI
	UI xUI;
	// Variable used to control the audio
	Audio xBGMAudio;
	// Variable used to control the animation
	Animation xAnimation;
};

#endif // _CT6025ASSIGNMENT_H
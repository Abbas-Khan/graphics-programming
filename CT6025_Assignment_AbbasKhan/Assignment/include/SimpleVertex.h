#ifndef _SIMPLEVERTEX_H
#define _SIMPLEVERTEX_H

///////////////////////////////////////////////////////////////////
// Name: SimpleVertex.h
// Author: Abbas Khan
// Bio: Class used to hold the base tech behind a simple vertex
///////////////////////////////////////////////////////////////////

// Required includes
#include <glm/glm.hpp>

// Simple Vertex Class
class SimpleVertex
{
public:
	// Enumeration to hold the offsets for the vertex
	enum Offsets
	{
		PositionOffset = 0,
		TexCoord1Offset = PositionOffset + sizeof(glm::vec4),
	};

	// Blank Constructor
	SimpleVertex();
	// Constructor with arguemnets to specify position & UV
	SimpleVertex(const glm::vec4& a_vec4Position, const glm::vec2 a_vec2UV);
	// Constructor with arguemnets to specify position & UV through a simple vertex
	SimpleVertex(const SimpleVertex& a_xSimpleVertex);
	// Destructor
	~SimpleVertex();

	// Comparison Operator overloads
	bool operator == (const SimpleVertex& a_xSimpleVertex_RHS) const;
	bool operator < (const SimpleVertex& a_xSimpleVertex_RHS) const;

	// Vector 4 to hold the position
	glm::vec4 vec4Position;
	// Vector 2 to hold the UV
	glm::vec2 vec2UV;
};

#endif // _SIMPLEVERTEX_H